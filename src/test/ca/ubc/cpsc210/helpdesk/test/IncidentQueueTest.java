package ca.ubc.cpsc210.helpdesk.test;

import ca.ubc.cpsc210.helpdesk.model.Incident;
import ca.ubc.cpsc210.helpdesk.model.IncidentQueue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IncidentQueueTest {
    private IncidentQueue testIncidentQueue;
    private Incident testIncident;

    @BeforeEach
    public void runBefore() {
        testIncidentQueue = new IncidentQueue();
        testIncident = new Incident(0,"Sleep Over The Test");
    }

    @Test
    public void testConstructor() {
        assertEquals(testIncidentQueue.length(),0);
    }

    @Test
    public void testAddIncident() {
        testIncidentQueue.addIncident(testIncident);
        assertEquals(testIncidentQueue.length(),1);
        assertEquals(testIncident,testIncidentQueue.getNextIncident());
    }

    @Test
    public void testGetNextIncident() {
        testIncidentQueue.addIncident(testIncident);
        assertEquals(testIncidentQueue.length(),1);
        assertEquals(testIncident,testIncidentQueue.getNextIncident());
    }

    @Test
    void testGetPositionInQueueOfCaseNumber() {
        for (int i = 1; i <= 20; i++) {
            Incident tempValue  = new Incident(i, "Miss Class !!!!");
            testIncidentQueue.addIncident(tempValue );
        }
        assertEquals(testIncidentQueue.getPositionInQueueOfCaseNumber(21), -1);
        assertEquals(testIncidentQueue.getPositionInQueueOfCaseNumber(3), 3);
    }

    @Test
    public void testLength() {
        assertEquals(testIncidentQueue.length(), 0);

        for (int i = 1; i <= 10; i++) {
            Incident tempValue  = new Incident(i, "Miss Class !!!!");
            testIncidentQueue.addIncident(tempValue );
        }
        assertEquals(testIncidentQueue.length(), 10);
    }

    @Test
    public void testIsEmpty() {
        assertTrue(testIncidentQueue.isEmpty());
        for (int i = 1; i <= 20; i++) {
            Incident tempValue  = new Incident(i,"Miss Class !!!!!");
            testIncidentQueue.addIncident(tempValue );
        }
        assertFalse(testIncidentQueue.isEmpty());
    }

    @Test
    public void testIsFull() {
        for (int i = 1; i < IncidentQueue.MAX_SIZE; i++) {
            Incident tempValue  = new Incident(i,"Miss Class !!!!!");
            testIncidentQueue.addIncident(tempValue );
        }
        assertFalse(testIncidentQueue.isFull());

        Incident tempValue  = new Incident(10,"Miss Class !!!!!");
        testIncidentQueue.addIncident(tempValue );
        assertTrue(testIncidentQueue.isFull());

        tempValue  = new Incident(11,"Miss Class !!!!!");
        testIncidentQueue.addIncident(tempValue );
        assertTrue(testIncidentQueue.isFull());
    }
}