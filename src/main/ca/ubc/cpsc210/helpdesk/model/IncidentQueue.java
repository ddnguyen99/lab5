package ca.ubc.cpsc210.helpdesk.model;

import java.util.LinkedList;

// Represents a queue of incidents to be handled by helpdesk
// with maximum size MAX_SIZE
public class IncidentQueue {
    public static final int MAX_SIZE = 10;
    // TODO: complete the design of the IncidentQueue class
    private LinkedList<Incident> incidents;

    public IncidentQueue() {
        incidents = new LinkedList<>();
    }

    public boolean addIncident(Incident incident) {
        if (incidents.size() >= MAX_SIZE) {
            return false;
        } else {
            incidents.addLast(incident);
            return true;
        }
    }

    public Incident getNextIncident() {
        Incident deleteValue = incidents.getFirst();
        incidents.removeFirst();
        return deleteValue;
    }

    public int getPositionInQueueOfCaseNumber(int caseNumber) {
        for (int i = 0; i < incidents.size(); ++i) {
            if (incidents.get(i).getCaseNum() == caseNumber) {
                return i + 1;
            }
        }
        return -1;
    }

    public int length() {
        return incidents.size();
    }

    public boolean isEmpty() {
        if (incidents.size() == 0) {
            return true;
        }
        return false;
    }

    public boolean isFull() {
        if (incidents.size() >= MAX_SIZE) {
            return true;
        } else {
            return false;
        }
    }
}
